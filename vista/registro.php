<!DOCTYPE html>
<html lang="en">

<head>

    <title>REGISTRO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap v5.1.3 CDNs -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    <!-- CSS File -->
    <link rel="stylesheet" href="public/css/style.css">

</head>
<style>
    body {
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background: linear-gradient(rgba(0, 0, 0, 0.6), rgba(1, 1, 1, 0.6)), url(public/img/utp.jpg);
    height: 70%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
   
}
</style>
<body>

    <div class="registro">

    <img class="d-block mx-auto mb-4" src="public/img/utp.svg" alt="" width="100" height="100">
    <center>
    <h1 class="h3 mb-3 font-weight-normal">Registrar Usuario</h1>
    <p class="<?php if (isset ($_GET['msg'])) echo $_GET['t'];?>"> <?php if (isset ($_GET['msg'])) echo $_GET['msg'];?> </p> 
</center>
    
        <form class="needs-validation form-signin" method="POST" action="./?op=registrar">
        <div class="form-group was-validated">
                <label class="form-label" for="text">nombre</label>
                <input class="form-control" type="nombre" id="nombre" name="txtName" required>
                <div class="invalid-feedback">
                    Por favor, introduzca su nombre
                </div>
            </div>
            <div class="form-group was-validated">
                <label class="form-label" for="text">apellido</label>
                <input class="form-control" type="apellido" id="apellido" name="txtApellido" required>
                <div class="invalid-feedback">
                    Por favor, introduzca su apellido
                </div>
            </div>
            <div class="form-group was-validated">
                <label class="form-label" for="email">Correo</label>
                <input class="form-control" type="email" id="correo" name="txtEmail" required>
                <div class="invalid-feedback">
                    Por favor, introduzca su correo
                </div>
            </div>
            <div class="form-group was-validated">
                <label class="form-label" for="password">Contraseña</label>
                <input class="form-control" type="password" id="contra"  name="txtPass" required>
                <div class="invalid-feedback">
                    Por favor, introduzca su contraseña
                </div>
            </div>
            <div class="form-group was-validated">
                <label class="form-label" for="password"> Repetir contraseña</label>
                <input class="form-control" type="password" id="contra2" name="txtPassrep" required>
                <div class="invalid-feedback">
                    Por favor, introduzca la contraseña
                </div>
            </div>
            <div class="form-group form-check">
                <input class="form-check-input" type="checkbox" id="check">
                <label class="form-check-label" for="check">Acepto las politicas y condiciones</label>
            </div>
            <input class="btn btn-success w-100" type="submit" value="REGISTRARSE" onClick="./?op=registrar">
            Ya tienes una cuenta -<a  href="./"> iniciar seccion</a>
        </form>

    </div>

</body>

</html>