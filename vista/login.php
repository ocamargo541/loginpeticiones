<!DOCTYPE html>
<html lang="en">

<head>

    <title>LOGIN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap v5.1.3 CDNs -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    <!-- CSS File -->
    <link rel="stylesheet" href="public/css/style.css">

</head>
<style>
    body {
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background: linear-gradient(rgba(0, 0, 0, 0.6), rgba(1, 1, 1, 0.6)), url(public/img/utp.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
   
}
</style>
<body>

    <div class="login">

    <img class="d-block mx-auto mb-4" src="public/img/utp.svg" alt="" width="100" height="100">

    <p class="text-danger"> <?php if (isset ($_GET['msg'])) echo $_GET['msg'];?> </p> 
        
        <form class="needs-validation" method="POST" action="./?op=acceder">
            <div class="form-group was-validated">
                <label class="form-label" for="email">Email address</label>
                <input class="form-control" type="email" id="correo"name="txtEmailLog"required>
                <div class="invalid-feedback">
                    Por favor, introduzca su correo
                </div>
            </div>
            <div class="form-group was-validated">
                <label class="form-label" for="password">Password</label>
                <input class="form-control" type="password" id="contraseña" name="txtPassLog" required>
                <div class="invalid-feedback">
                    Por favor, introduzca su contraseña
                </div>
            </div>
            <div class="form-group form-check">
                <input class="form-check-input" type="checkbox" id="check">
                <label class="form-check-label" for="check">Recuerdame</label>
            </div>
            <input class="btn btn-success w-100" type="submit" value="INICIAR" onClick="./?op=acceder">
            <a href="?op=crear"> No tienes cuenta? registrate</a>
        </form>
<script>
    
</script>
    </div>

</body>

</html>