<?php

if ($_SESSION["acceso"] != true)
{
    header('Location: ?op=error');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menú lateral responsive - MagtimusPro</title>

    <link rel="stylesheet" href="public/css/estilo.css">

    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>
</head>
<body id="body">
    
    <header>
        <div class="icon__menu">
            <i class="fas fa-bars" id="btn_open"></i>
            
        </div>

        <div class="menu_usuario" >
               <li>
                <a class="profile-pic" href="?op=perfil">
                <img src="public/img/users/<?php echo $_SESSION["foto"]; ?>" alt="user-img" width="36"
                class="img-circle"><span class="text-white font-medium"><?php echo $_SESSION["user"] ?></span></a>
                </li>
</div>
    </header>

    <div class="menu__side" id="menu_side">

        <div class="name__page">
            <img src="public/img/utp.svg"alt="homepage" width="30" height="30">
            <h4>UTP Admin</h4>
        </div>

        <div class="options__menu">	

            <a href="#" class="selected">
                <div class="option">
                    <i class="fas fa-home" title="Inicio"></i>
                    <h4>Dashboard</h4>
                </div>
            </a>

            <a href="?op=perfil">
                <div class="option">
                    <i class="far fa-file" title="Perfil"></i>
                    <h4>Perfil</h4>
                </div>
            </a>
            
            <a href="#">
                <div class="option">
                    <i class="fas fa-video" title="Cursos"></i>
                    <h4>Cursos</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="far fa-sticky-note" title="Blog"></i>
                    <h4>Blog</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="far fa-id-badge" title="Contacto"></i>
                    <h4>Contacto</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="far fa-address-card" title="Nosotros"></i>
                    <h4>Nosotros</h4>
                </div>
            </a>

        </div>

    </div>

    <section>
    <main>
    <div class="main-container">
                                <table class="">
                                    <thead>
                                        <tr>
                                            <th class="">#</th>
                                            <th class="">Nombre</th>
                                            <th class="">Apellido</th>
                                            <th class="">Email</th>
                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $n=1;
                                    foreach ($listaUsuario as $lista) {
                                    ?>
                                        <tr>
                                            <td ><?php echo $n; ?></td>
                                            <td><?php echo $lista->nombre; ?></td>
                                            <td><?php echo $lista->apellido; ?></td>
                                            <td><?php echo $lista->email; ?></td>
                                            
                                        </tr>
                                    <?php
                                        $n++;
                                    }
                                    ?> 
                                    </tbody>
                                </table>




                            </div>
 </main>
    </section>
    <script src="public/js/script.js"></script>
</body>
</html>