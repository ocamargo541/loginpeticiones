<?php
@session_start();// Comienzo de la sesión

if ($_SESSION["acceso"] != true)
{
    header('Location: ?op=error');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Ample lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Ample admin lite dashboard bootstrap 5 dashboard template">
    <meta name="description"
        content="Ample Admin Lite is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
    <title>Menú lateral responsive - MagtimusPro</title>

    <!-- Favicon icon -->
    <link rel="stylesheet" href="public/css/estilo.css">
    <link rel="stylesheet" href="public/css/perfil.css">
    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>
    
  
</head>
<body id="body">
    
    <header>
        <div class="icon__menu">
            <i class="fas fa-bars" id="btn_open"></i>
            
        </div>

        <div class="menu_usuario" >
               <li>
                <a class="profile-pic" href="?op=perfil">
                <img src="public/img/users/<?php echo $_SESSION["foto"]; ?>" alt="user-img" width="36"
                class="img-circle"><span class="text-white font-medium"><?php echo $_SESSION["user"] ?></span></a>
                </li>
</div>
    </header>
    <script>

var arrayIdDistrito = new Array();
var arrayNomDistrito = new Array();
var arrayIdProvincia = new Array();
var i=1;
// Inicializamos 3 arreglos con los datos de los Ids de las provincias y distritos; ademas, del nombre de los distritos
<?php
$n=0;
foreach ($distritos as $d)  {
    echo "arrayIdDistrito[$n]=$d->id_distrito;";
    echo "arrayNomDistrito[$n]='$d->nom_distrito';";
    echo "arrayIdProvincia[$n]=$d->id_provincia;";
    $n++;
    }
 ?>
var n ="<?php echo $n; ?>"; //total de registros

   function SelectDistrito()
{
    var indice=0;
    var valor=0;
    //asignamos a la variable valor el valor de la lista de menú seleccionado
    valor=document.formulario.provincia.value;
    document.formulario.distrito.length=0; //Limpiamos la lista de menu distrito
    for (x=0;x<n;x++) {

        if (valor == arrayIdProvincia[x]  )
        {
        //asigna a la lista de menú sub_areas los nuevos valores segun la selección del menú areas
        document.formulario.distrito[indice]= new Option(arrayNomDistrito[x],arrayIdDistrito[x]);
        indice=indice+1;
        }
        }

  }	
</script>

    <div class="menu__side" id="menu_side">

        <div class="name__page">
            <img src="public/img/utp.svg"alt="homepage" width="30" height="30">
            <h4>UTP Admin</h4>
        </div>

        <div class="options__menu">	

            <a href="?op=permitido">
                <div class="option">
                    <i class="fas fa-home" title="Inicio"></i>
                    <h4>Dashboard</h4>
                </div>
            </a>

            <a href="#" class="selected">
                <div class="option">
                    <i class="far fa-file" title="Portafolio"></i>
                    <h4>Perfil</h4>
                </div>
            </a>
            
            <a href="#">
                <div class="option">
                    <i class="fas fa-video" title="Cursos"></i>
                    <h4>Cursos</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="far fa-sticky-note" title="Blog"></i>
                    <h4>Blog</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="far fa-id-badge" title="Contacto"></i>
                    <h4>Contacto</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="far fa-address-card" title="Nosotros"></i>
                    <h4>Nosotros</h4>
                </div>
            </a>

        </div>

    </div>

    <div class="container-perfil">
    <main>

    <div class="col-lg-8 col-xlg-9 col-md-12">

                        <div class="white-box">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"><img src="public/img/users/<?php echo $_SESSION["foto"]; ?>"
                                                class="thumb-lg img-circle" alt="img"></a>
                                        <h4 class="text-white mt-2"><?php echo $usuario->nombre. " ".$usuario->apellido; ?></h4>
                                        <h5 class="text-white mt-2"><?php echo $usuario->email; ?></h5>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                        <div class="card">
                            <div class="card-body">
                                <form class="form-horizontal form-material" name="formulario" method="POST" action="./?op=actualizar" enctype="multipart/form-data">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Nombre</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input type="text" name ="nombre"  class="form-control p-0 border-0" value="<?php echo $usuario->nombre; ?>"required> </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Apellido</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input type="text" name ="apellido"  class="form-control p-0 border-0" value="<?php echo $usuario->apellido; ?>"required> </div>
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="example-email" class="col-md-12 p-0">Email</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input type="email" value="<?php echo $usuario->email; ?>"
                                                class="form-control p-0 border-0" name="correo"
                                                id="example-email" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="example-email" class="col-md-12 p-0">Provincia</label>
                                        <div class="col-md-12 border-bottom p-0">
                                        <select name="provincia" id="provincia" onchange="SelectDistrito()">
                                        <?php foreach ($provincia as $p){ ?>
                                            <option value="<?php echo $p->id_provincia; ?>"><?php echo $p->nom_provincia; ?></option>
                                        <?php } ?>      
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="example-email" class="col-md-12 p-0">Distrito</label>
                                        <div class="col-md-12 border-bottom p-0">
                                        <select name="distrito" id="distrito">
                                        </select>
                                        </div>
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="example-foto" class="col-md-12 p-0">Cambiar Foto</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input accept="image/*" type="file" class="form-control p-0 border-0" name="foto"
                                                id="foto" >
                                    </div>
                                    </div>

                                    <div class="form-group mb-4">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success">Actualizar Datos</button>
                                        </div>
                                    </div>

                                    <input id="oculto" name="fotoActual" type="hidden" value="">


                                </form>
                            </div>
                        </div>
                    </div>
    
    </main></div>
    <script src="public/js/script.js"></script>
    <script src="public/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="public/js/bootstrap.bundle.min.js"></script>
    <script src="public/js/app-style-switcher.js"></script>
    <!--Wave Effects -->
    <script src="public/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="public/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="public/js/custom.js"></script>
</body>
</html>