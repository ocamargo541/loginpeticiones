<?php
session_start();// Comienzo de la sesión
require_once 'modelo/usuario.php';
require_once 'modelo/ubicacion.php';
class Control
{
    private $model;
    private $model2;
    private $model3;
    private $model4;
    private $resp;

    public function __CONSTRUCT(){
        $this->model = new Usuario();
        $this->model2 = new Ubicacion();
        $this->model3 = new Ubicacion();
        $this->model4 = new Usuario();
    }

    public function index(){
        require("vista/login.php");
    }



    public function registarUsuario(){
        require("vista/registro.php");

    }

    public function IngresarPanel(){
        $listaUsuario = new Usuario();
        $listaUsuario = $this->model4->ObtenerTodosLosUsuarios();

        require("vista/panel/dashboard.php");

    }

    public function IngresarPerfil(){

        $usuario = new Usuario();
        $usuario = $this->model->Obtener($_SESSION['id']);
 
        $provincia =new Ubicacion();
        $provincia= $this->model2->ConsultarProvincia();

        $distritos =new Ubicacion();
        $distritos= $this->model3->ConsultarDistrito();

        require("vista/panel/profile.php");
    }

    public function Guardar(){
        $usuario = new Usuario();
        $usuario->nombre = $_REQUEST['txtName'];
        $usuario ->apellido = $_REQUEST['txtApellido'];
        $usuario ->email = $_REQUEST['txtEmail'];
        $usuario ->pass = md5($_REQUEST['txtPass']);

        $this->resp=$this->model->Registrar($usuario);

        header('Location: ?op=crear&msg='.$this->resp);
    }

    public function Ingresar(){
        $ingresarUsuario = new Usuario();
         
        $ingresarUsuario->email = $_REQUEST['txtEmailLog'];
        $ingresarUsuario->pass = md5($_REQUEST['txtPassLog']);

        if($resultado=$this->model->Consultar($ingresarUsuario))
        {
            $_SESSION["acceso"]=true;
            $_SESSION["foto"] = $resultado->foto;
            $_SESSION["id"] = $resultado->id;
            $_SESSION["user"]=$resultado->nombre." ".$resultado->apellido;
            header('Location: ?op=permitido');
        }else{
            header("Location: ?&msg=Su contraseña o usuario está incorrecto");
        }

    }



    public function ActualizarDatos(){
        $usuario = new Usuario();
        $usuario->nombre = $_REQUEST['nombre'];
        $usuario->apellido = $_REQUEST['apellido'];
        $usuario->id_distrito= $_REQUEST['distrito'];
        $usuario->id=$_SESSION["id"];

        if (isset($_FILES['foto']))
        {
            move_uploaded_file($_FILES['foto']['tmp_name'], "./public/img/users/".$_SESSION["id"].".jpg");
            
            $usuario->foto = $_SESSION["id"].".jpg";

            $_SESSION["foto"]=$_SESSION["id"].".jpg";
        }
        else
        {
            $usuario->foto = $_SESSION["foto"];
        }
         
        
        $this->resp= $this->model->Actualizar($usuario);

        header('Location: ?op=perfil&msg='.$this->resp);

    }


}
